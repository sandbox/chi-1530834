(function ($) {
Drupal.behaviors.contenteditable = {
  attach: function (context, settings) {

    // The onchange event doesn't fires when an element with
    // the contentEditable attribute is changed.
    // The code below doesn't process changing content edited by drag and drop
    // or by using the browser's context menu.
    // See http://stackoverflow.com/a/6263537/272927
    $('.contenteditable').live('focus', function() {
      var $this = $(this);
      $this.data('before',  $this.html());
    }).live('blur keyup paste', function() {
      var $this = $(this);
      if ($this.data('before') !== $this.html()) {
        $this.data('before', $this.html());
        $this.next('input').val($this.html())
      }
    })

  }
};
})(jQuery);
